''' searchit.database - SQLite Database '''

import logging
import sqlite3
import yaml

# Database

class Database(object):
    SQLITE_PATH = 'searchit.db'
    YAML_PATH   = 'assets/yaml/data.yaml'

    def __init__(self, data=YAML_PATH, path=SQLITE_PATH):
        # TODO: Set instance variables and call _create_tables and _load_tables
        self.logger = logging.getLogger()
        self.data   = data
        self.path   = path
        self.conn   = sqlite3.connect(self.path)

        self._create_tables()
        self._load_tables()

    def _create_tables(self):
        # TODO: Create Artist, Album, and Track tables
        with self.conn:
            cursor = self.conn.cursor()
            artist_sql = '''
            CREATE TABLE IF NOT EXISTS Artists(
            ArtistID INTEGER NOT NULL,
            Name TEXT NOT NULL,
            Image TEXT NOT NULL,
            PRIMARY KEY(ArtistID)
            )    
            '''
            album_sql='''
            CREATE TABLE IF NOT EXISTS Albums(
            ArtistID INTEGER NOT NULL,
            AlbumID INTEGER NOT NULL,
            Name Text NOT NULL,
            Image TEXT NOT NULL,
            PRIMARY KEY(AlbumID),
            FOREIGN KEY(ArtistID) REFERENCES Artists(ArtistID) 
            )
            '''
            track_sql = '''
            CREATE TABLE IF NOT EXISTS Tracks(
            AlbumID INTEGER NOT NULL,
            TrackID INTEGER NOT NULL,
            Number INTEGER NOT NULL,
            Name TEXT NOT NULL,
            PRIMARY KEY(TrackID),
            FOREIGN KEY(AlbumID) REFERENCES Albums(AlbumID)
            )
            '''
            
            cursor.execute(artist_sql)
            self.logger.info('Created Artists Table')

            cursor.execute(album_sql)
            self.logger.info('Created Albums Table')

            cursor.execute(track_sql)
            self.logger.info('Created Tracks Table')    

    def _load_tables(self):
        # TODO: Insert Artist, Album, and Track tables from YAML
        with self.conn:
            ArtistID = 1
            TrackID = 1
            AlbumID = 1
            cursor = self.conn.cursor()
            for artist in yaml.load(open(self.data)):
                Name = artist['name']
                Image = artist['image']
                artist_sql = 'INSERT OR REPLACE INTO Artists(ArtistID, Name, Image) VALUES (?, ?, ?)' 
                cursor.execute(artist_sql, (ArtistID, Name, Image))
                self.logger.debug('Added Artist: id = {}, name = {}' .format(ArtistID, artist['name']))
               

                for album in artist['albums']:
                    Name = album['name']
                    Image = album['image']
                    album_sql = 'INSERT OR REPLACE INTO Albums(ArtistID, AlbumID, Name, Image) VALUES (?, ?, ?, ?)'
                    cursor.execute(album_sql, (ArtistID, AlbumID, Name, Image))
                    self.logger.debug('Added Album: artist = {}, id = {}, Name = {}' .format(ArtistID, AlbumID, artist['albums']))

                    Number = 1
                    for track in album['tracks']:
                        Name = track
                        track_sql = 'INSERT OR REPLACE INTO Tracks(AlbumID, TrackID, Number, Name) VALUES (?, ?, ?, ?)'
                        cursor.execute(track_sql, (AlbumID, TrackID, Number, Name))
                        self.logger.debug('Added Track: album = {}, id = {}, number = {}, name = {}' .format(AlbumID, TrackID, Number, Name))
                        Number += 1
                        TrackID += 1

                    AlbumID += 1

                ArtistID += 1

    def artists(self, artist):
        # TODO: Select artists matching query
        with self.conn:
            cursor = self.conn.cursor()
            artist_sql = '''SELECT ArtistID, Name, Image FROM Artists WHERE Name LIKE ?
                            ORDER BY Name'''

            return cursor.execute(artist_sql, ('%{}%'.format(artist),))


    def artist(self, artist_id=None):
        # TODO: Select artist albums
        with self.conn:
            cursor = self.conn.cursor()
            artist_sql = '''SELECT AlbumID, Name, Image From Albums WHERE ArtistID = ?'''

            return cursor.execute(artist_sql, (artist_id,))

    def albums(self, album):
        # TODO: Select albums matching query
        with self.conn:
            cursor = self.conn.cursor()
            album_sql = '''SELECT AlbumID, Name, Image FROM Albums WHERE Name LIKE ?
                           ORDER BY Name'''

            return cursor.execute(album_sql, ('%{}%'.format(album),))

    def album(self, album_id=None):
        # TODO: Select specific album
        with self.conn:
            cursor = self.conn.cursor()
            album_sql = '''SELECT TrackID, Number, Name From Tracks WHERE AlbumID = ?'''

            return cursor.execute(album_sql, (album_id,))

    def tracks(self, track):
        # TODO: Select tracks matching query
        with self.conn:
            cursor = self.conn.cursor()
            track_sql = '''SELECT TrackID, Tracks.Name, Albums.Image FROM Tracks
                        JOIN Albums ON Tracks.AlbumID = Albums.AlbumID
                        WHERE Tracks.Name LIKE ?
                        ORDER BY Tracks.Name
                        '''
            return cursor.execute(track_sql, ('%{}%'.format(track),))

    def track(self, track_id=None):
        # TODO: Select specific track
        with self.conn:
            cursor = self.conn.cursor()
            track_sql = '''SELECT TrackID, Artists.ArtistID, Artists.Name, Albums.AlbumID, Albums.Name, Number, Tracks.Name FROM Tracks
                        JOIN Albums on Tracks.AlbumID = Albums.AlbumID
                        JOIN Artists on Albums.ArtistID = Artists.ArtistID
                        WHERE TrackID = ?
                        ORDER BY Tracks.TrackID'''

            return cursor.execute(track_sql, (track_id,))

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
