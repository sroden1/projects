''' searchit.web - Web Application '''

from searchit.database import Database

import logging

import tornado.ioloop
import tornado.web

# Application

class Application(tornado.web.Application):
    DEFAULT_PORT  = 9955
    TEMPLATE_PATH = 'assets/html'

    def __init__(self, port=DEFAULT_PORT):
	tornado.web.Application.__init__(self, debug=True, template_path=Application.TEMPLATE_PATH)
        # TODO: Initialize database and port
        self.logger   = logging.getLogger()
        self.ioloop   = tornado.ioloop.IOLoop.instance()
        self.database = Database()
        self.port     = port

        self.add_handlers('', [
            (r'/'         , IndexHandler),
            (r'/artist/(.*)'   , ArtistHandler),
            (r'/album/(.*)'    , AlbumHandler),
            (r'/track/(.*)'    , TrackHandler),
            ])

        # TODO: Add Index, Album, Artist, and Track Handlers

    def run(self):
        try:
            self.listen(self.port)
        except socket.error as e:
            self.logger.fatal('Unable to listen on {} = {}'.format(self.port, e))
            sys.exit(1)

        self.ioloop.start()

# Handlers

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        # TODO: Implement Index Handler
        table = self.get_argument('table', '')
        query = self.get_argument('query', '')

        if query == '':
            self.render('index.html')


        if table == 'Artists':
            results = self.application.database.artists(query)
            index = 'artist'
        elif table == 'Albums':
            results = self.application.database.albums(query)
            index = 'album'
        elif table == 'Tracks':
            results = self.application.database.tracks(query)
            index = 'track'

        if table == None:
            self.render('index.html', query=query)
        else:
            self.render('gallery.html', results=results, prefix=index)


class ArtistHandler(tornado.web.RequestHandler):
    def get(self, artist_id=None):
        # TODO: Implement Artist Handler
        if artist_id: 
            results = self.application.database.artist(artist_id)
            self.render('gallery.html', results=results, prefix='album')
        else: 
            artist = ''
            results = self.application.database.artists(artist) #how do we generate an empty string??
            self.render('gallery.html', results=results, prefix='artist')


class AlbumHandler(tornado.web.RequestHandler):
    def get(self, album_id=None):
        # TODO: Implement Album Handler
        if album_id:
            results = self.application.database.album(album_id)
            self.render('album.html', results=results, prefix='track')
        else: 
            album = ''
            results = self.application.database.albums(album) #generate an empty string??
            self.render('gallery.html', results=results, prefix='album')

class TrackHandler(tornado.web.RequestHandler):
    def get(self, track_id=None):
        # TODO: Implement Album Handler

        if track_id:
            results = self.application.database.track(track_id)
            self.render('track.html', results=results, prefix='track')
        else:
            track = ''
            results = self.application.database.tracks(track)
            self.render('gallery.html', results=results, prefix='track')

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
