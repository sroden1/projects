''' playit.web - Web Application '''

from final.database import Database

import sys
import json
import logging
import socket
import socket

import tornado.ioloop
import tornado.web

# Application

class Application(tornado.web.Application):
    DEFAULT_PORT  = 9842
    TEMPLATE_PATH = 'assets/html'

    def __init__(self, port=DEFAULT_PORT):
	tornado.web.Application.__init__(self, debug=True, template_path=Application.TEMPLATE_PATH)
        self.logger   = logging.getLogger()
        self.database = Database()
        self.ioloop   = tornado.ioloop.IOLoop.instance()
        self.port     = port

        # TODO: Add Index, Album, Artist, and Track Handlers
        self.add_handlers('', [
            (r'/'           , IndexHandler),
            (r'/search'     , SearchHandler),
            (r'/student/(.*)', StudentHandler),
            (r'/movie/(.*)', MovieHandler),
            (r'/actor/(.*)' , ActorHandler),
            (r'/year/(.*)' , YearHandler),
            (r'/assets/(.*)', tornado.web.StaticFileHandler, {'path': 'assets'}),
        ])

    def run(self):
        try:
            self.listen(self.port)
        except socket.error as e:
            self.logger.fatal('Unable to listen on {} = {}'.format(self.port, e))
            sys.exit(1)

        self.ioloop.start()

# Handlers

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('final.html')

class StudentHandler(tornado.web.RequestHandler):
    def get(self, student_id=None):
        self.write(json.dumps({
            'render': 'gallery',
            'prefix': 'Movie',
            'results': list(self.application.database.student(student_id)),
            }))


class SearchHandler(tornado.web.RequestHandler):
    def get(self):
        # TODO: Implement Index Handler
        query   = self.get_argument('query', '')
        table   = self.get_argument('table', '')
        results = []

        if table == 'Movies':
            results = self.application.database.movies(query)
        elif table == 'Actors':
            results = self.application.database.actors(query)
        elif table == 'Years':
            results = self.application.database.years(query)

        self.write(json.dumps({
            'render' : 'gallery',
            'prefix' : table[:-1],
            'results': list(results),
        }))

class MovieHandler(tornado.web.RequestHandler):
    def get(self, movie_id=None):
        # TODO: Implement Artist Handler
        if not movie_id:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'Movie',
                'results': list(self.application.database.movies('')),
            }))
        else:
            self.write(json.dumps({
                'render' : 'movie',
                'results': list(self.application.database.movie(movie_id)),
            }))

class ActorHandler(tornado.web.RequestHandler):
    def get(self, actor_id=None):
        # TODO: Implement Album Handler
        if not actor_id:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'Actor',
                'results': list(self.application.database.actors('')),
            }))
        else:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'Movie',
                'results': list(self.application.database.actor(actor_id)),
            }))

class YearHandler(tornado.web.RequestHandler):
    def get(self, year_id=None):
        # TODO: Implement Track Handler
        if not year_id:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'Movie',
                'results': list(self.application.database.years('')),
            }))
        else:
            self.write(json.dumps({
                'render' : 'year',
                'results': list(self.application.database.year(year_id)),
            }))

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
