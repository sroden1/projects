''' final.database - SQLite Database '''

import logging
import sqlite3
import yaml

# Database

class Database(object):
    SQLITE_PATH = 'final.db'
    YAML_PATH   = 'assets/yaml/final.yaml'

    def __init__(self, data=YAML_PATH, path=SQLITE_PATH):
        self.logger = logging.getLogger()
        self.data   = data
        self.path   = path
        self.conn   = sqlite3.connect(self.path)

        self._create_tables()
        self._load_tables()

    def _create_tables(self):
        students_sql = '''
        CREATE TABLE IF NOT EXISTS Students (
            StudentID   INTEGER NOT NULL,
            Name        TEXT    NOT NULL,
            PRIMARY KEY (StudentID)
        )
        '''

        movies_sql = '''
        CREATE TABLE IF NOT EXISTS Movies (
            StudentID   INTEGER NOT NULL,
            MovieID     INTEGER NOT NULL,
            Name        TEXT    NOT NULL,
            Image       TEXT    NOT NULL,
            Director    TEXT    NOT NULL,
            Trailer     TEXT    NOT NULL,
            YearName    TEXT    NOT NULL,
            PRIMARY KEY (MovieID),
            FOREIGN KEY (StudentID) REFERENCES Students(StudentID)
        )
        '''

        actors_sql = '''
        CREATE TABLE IF NOT EXISTS Actors(
            MovieID     INTEGER NOT NULL,
            ActorID     INTEGER NOT NULL,
            Name        TEXT NOT NULL,
            Image       TEXT NOT NULL,
            PRIMARY KEY (ActorID, MovieID),
            FOREIGN KEY (MovieID) REFERENCES Movies(MovieID)
        )
        '''

        years_sql = '''
        CREATE TABLE IF NOT EXISTS Years(
            YearID      INTEGER NOT NULL,
            YearName    TEXT NOT NULL,
            MovieID     INTEGER NOT NULL,
            PRIMARY KEY (YearID),
            FOREIGN KEY (MovieID) REFERENCES Movies(MovieID)
            )
        '''

        with self.conn:
            cursor = self.conn.cursor()
            cursor.execute(students_sql)
            cursor.execute(movies_sql)
            cursor.execute(actors_sql)
            cursor.execute(years_sql)

    def _load_tables(self):
        students_sql = 'INSERT OR REPLACE INTO Students (StudentID, Name) VALUES (?, ?)'
        movies_sql  = 'INSERT OR REPLACE INTO Movies  (StudentID, MovieID, Name, Image, Director, Trailer, YearName) VALUES (?, ?, ?, ?, ?, ?, ?)'
        actors_sql  = 'INSERT OR REPLACE INTO Actors  (MovieID, ActorID, Name, Image) VALUES (?, ?, ?, ?)'
        years_sql   = 'INSERT OR REPLACE INTO Years (YearID, YearName, MovieID) VALUES (?, ?, ?)'

        with self.conn:
            cursor    = self.conn.cursor()
            student_id = 1
            movie_id  = 1
            actor_id  = 1
            year_id = 1

            for student in yaml.load(open(self.data)):
                cursor.execute(students_sql, (student_id, student['name']))
                self.logger.debug('Added Student: id={}, name={}'.format(student_id, student['name']))

                for movie in student['movies']:
                    cursor.execute(movies_sql, (student_id, movie_id, movie['name'], movie['image'], movie['director'], movie['trailer'], movie['year']))
                    self.logger.debug('Added Movie: id={}, name={}'.format(movie_id, movie['name']))

                    for actor in movie['actors']:
                        cursor.execute(actors_sql, (movie_id, actor_id, actor['name'], actor['image']))
                        self.logger.debug('Added Actor: id={}, name={}'.format(actor_id, actor['name']))
                        actor_id += 1

                    for year in movie['year']:
                        cursor.execute(years_sql, (year_id, movie['year'], movie_id))
                        self.logger.debug('Added Year: id={}, name={}'.format(actor_id, movie['year']))
                    movie_id += 1
                    year_id  += 1

                student_id += 1

    def student(self, student_id=None):
        student_sql = '''
        SELECT      MovieID, Name, Image
        FROM        Movies
        WHERE       StudentID = ?
        ORDER BY    Name
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(student_sql, (student_id,))

    def movies(self, movie):
        movies_sql = 'SELECT MovieID, Name, Image FROM Movies WHERE Name LIKE ? ORDER BY Name'
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(movies_sql, ('%{}%'.format(movie),))

    def movie(self, movie_id=None):
        movies_sql = '''
        SELECT      Movies.MovieID, Movies.Name, Movies.Image, Director, GROUP_CONCAT(Actors.Name) AS Names, ActorID, Trailer, YearName
        FROM        Movies
        JOIN        Actors
        ON          Movies.MovieID = Actors.MovieID
        WHERE       Movies.MovieID = ?
        ORDER BY    Movies.Name
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(movies_sql, (movie_id,))

    def actors(self, actor):
        actors_sql = '''
        SELECT      ActorID, Name, Image
        FROM        Actors
        WHERE       Actors.Name LIKE ?
        ORDER BY    Name
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(actors_sql, ('%{}%'.format(actor),))

    def actor(self, actor_id=None):
        actor_sql = '''
        SELECT      Movies.MovieID, Movies.Name, Movies.Image, ActorID
        FROM        Actors
        JOIN        Movies
        ON          Movies.MovieID = Actors.MovieID
        WHERE       ActorID = ?
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(actor_sql, (actor_id,))

    def years(self, year):
        years_sql = '''
        SELECT      Movies.MovieID, Name, Image
        FROM        Movies
        JOIN        Years
        ON          Movies.MovieID = Years.MovieID
        WHERE       Years.YearName LIKE ?
        ORDER BY    Name
        '''

        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(years_sql, ('%{}%'.format(year),))

    def year(self, year_id=None):
        year_sql = '''
        SELECT      YearID, Movies.MovieID, YearName, Image
        FROM        Movies
        JOIN        Years
        ON          Movies.MovieID = Years.MovieID
        WHERE       YearID = ?
        ORDER BY    Name
        '''

        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(years_sql, (year_id,))


# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
