Final Project
==================

1. Our project is to make a website with our favorite movies. The user will be able to search within our movies according to
the movie title, actors, and year. Each movie will have information that goes along with it (in the yaml file), including a
movie trailer that will be embedded into the site.

- The data will be stored in a yaml file that will consist of each our names and 5 movies each. Each movie will have a title, year, 2-5
actors, director, image, and trailer.

- Picture of mockup drawing of the interface is in separate file.

- Storyboard: Our application will have 4 pages. The home/search page will have all 15 movies (image and title) and the ability to 
search (with a dropdown menu). There will then be a page for each of our names, with the a search box and dropdown menu on each.
For example, the user could click on the "Dan" tab and would be taken to the gallery of Dan's favorite movies. The user could then
search for a year, actor, or movie title. If a year is clicked on, all the movies released in that year will appear. If an actor is
clicked, all the movies that actor has appeared in will appear. If a movie title is clicked, more information about the movie will
appear, including a trailer.

2. Our project meets all of the requirements above: Tornado is used as the webframework for this web application. Tornado is the server
that receive the requests. SQLite is used as the server side database in order to create tables of the information provided in the
yaml file. Bootstrap is used to provide client side user interface to give a framework to our webpages. JavaScript allows for 
our website to have user interactivty. AJAX is used with JSON in order to retreive our data from the client side. Our files in our
final project folder will include: final.yml, final.css, final.html, final.js, web.py, __init__.py, and __main__.py.

3.Each person is responsible for putting together their own section of the yaml file with his/her 5 movies. We will work together
to do the rest.

4. No questions or issues, yet.
