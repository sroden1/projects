var CurrentNotebook = 'Search';	    /* Current notebook */

/* AJAX functions */

function requestJSON(url, callback) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
    	if (request.readyState == 4 && request.status == 200) {
    	    callback(JSON.parse(request.responseText));
	}
    }

    request.open('GET', url, true);
    request.send();
}

/* Display functions */

function displayTab() {
    /* TODO: Remove the active class from each notebook tab */
    var url = document.getElementsByClassName("nav-tabs")[0].getElementsByTagName("li");
    for (var i = 0; i < url.length; i++) {
        url[i].classList.remove("active");
    }


    /* TODO: Add active class to current object */
    this.classList.add("active");

    /* TODO: Check current object's id and call the appropriate display function */
    if (this.id === "search") {
        displaySearch();
    }
    else if (this.id === "sam") {
        displaySam();
    }
    else if (this.id === "dan") {
        displayDan();
    }
    else if (this.id === "nikki") {
        displayNikki();
    }
}

    /* TODO: Request JSON from URL and then call appropriate render function */
function displayResults(url) {
    requestJSON(url, function(data) {
        if (data['render'] == 'gallery') {
            renderGallery(data['results'], data['prefix']);
        } else if (data['render'] == 'movie') {
            renderMovie(data['results']);
        }
    });
}

function displayMovie(movie) {
    displayResults('/movie/' + movie)
}

function displayActor(actor){
    displayResults('/actor/' + actor)
}

function displayYear(movie){
    displayResults('/movie/' + movie)
}

function displaySearch(query) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = 'Search';

    var element    = document.getElementById('notebook-body-nav');
    var renderHTML = '<form class="navbar-form text-center">';

    renderHTML += '<div class="form-group">';
    renderHTML += '<input type="text" placeholder="What to search for" name="query" id="query" onkeyup="updateSearchResults()" class="form-control">';
    renderHTML += '<select id="table" name="table" class="form-control" onchange="updateSearchResults()">';
    renderHTML += '<option value="Movies">Movies</option>';
    renderHTML += '<option value="Actors">Actors</option>';
    renderHTML += '<option value="Years">Years</option>';
    renderHTML += '</select>';
    renderHTML += '</div></form>';

    element.innerHTML = renderHTML;
    return updateSearchResults();
}

function displaySam(sam) {
    CurrentNotebook = 'Sam';

    if (sam===undefined) {
        sam='';
    }

    displayResults('/student/' + 3);
}

function displayDan(dan) {
    CurrentNotebook = 'Dan';

    if (dan===undefined) {
        dan='';
    }

    displayResults('/student/' + 1);
}

function displayNikki(nikki) {

    CurrentNotebook = 'Nikki';
    if (nikki===undefined) {
        nikki='';
    }

    displayResults('/student/' + 2);
}

/* Render functions */

function renderGallery(data, prefix) {

    var element    = document.getElementById('notebook-body-contents');
    var renderHTML = '';

    renderHTML += '<div class="row">';

    for (var i=0; i < data.length; i++) {
        var item = data[i];
        var id = item[0];
        var name = item[1];
        var image = item[2];
        if (i % 4 == 0) {
            renderHTML += '</div>';
            renderHTML += '<div class="row">';
        }
        renderHTML += '<div class="col-xs-3">';
        renderHTML += '<a href="#" onclick="display' + prefix + '(' + id + ')' + '" class="thumbnail text-center">';
        renderHTML += '<img src=' + image + ' height="200" width="200">';
        renderHTML += '<div class="caption"><h4>' + name + '</h4></div>';
        renderHTML += '</a>';
        renderHTML += '</div>';
    }
    renderHTML += '</div>';

    element.innerHTML = renderHTML;
}

function renderMovie(data) {

  var element    = document.getElementById('notebook-body-contents');
  var renderHTML = '';

  renderHTML += '<table class="table table-striped">';
  renderHTML += '<thead>';
  renderHTML += '<th>Image</th>';
  renderHTML += '<th>Name</th>';
  renderHTML += '<th>Year</th>';
  renderHTML += '<th>Director</th>';
  renderHTML += '<th>Actors</th>';
  renderHTML += '</thead>';
  renderHTML += '<tbody>';
  for (var i=0; i < data.length; i++) {
    var item = data[i];
    var MovieID = item[0];
    var Name = item[1];
    var Image = item[2];
    var Director = item[3];
    var Actors = item[4];
    var ActorID = item[5];
    var Trailer = item[6];
    var Year = item[7];
    renderHTML += '<tr>';
    renderHTML += '<td>' + '<img width="50" src="' + Image + '">' + '</td>';
    renderHTML += '<td>' + Name + '</td>';
    renderHTML += '<td>' + Year + '</td>'; 
    renderHTML += '<td>' + Director + '</td>';
    renderHTML += '<td>' + Actors + '</td>';
    renderHTML += '</tr>';
    }
  renderHTML += '</tbody>';
  renderHTML += '</table>';
  renderHTML += '<iframe width="500" height="290" align="center" src="' + Trailer + '" frameborder="0" allowfullscreen></iframe>'

element.innerHTML = renderHTML;

}


/* Update functions */

function updateSearchResults() {
    /* TODO: Construct search URL from query and table values and call
     * displayResults
     */

     var query = document.getElementById('query').value;
     var table = document.getElementById('table').value;
     var url = "/search?query=" + query + "&table=" + table

     displayResults(url);
}


/* Registrations */
window.onload = function() {
    /* TODO: Register displayTab function for each tab's onclick callback */
    var url = document.getElementsByClassName("nav-tabs")[0].getElementsByTagName("li");
    for (var i = 0; i < url.length; i++) {
        url[i].onclick=displayTab;
    }

    displaySearch();
}