#! user/bin/env python

from drawit import Image, PPM, Point, Color, RED

image = Image(100, 100)

point0 = Point(10,10)
point1 = Point(10, 20)
point2 = Point(15, 15)

#Letter D
image.draw_line(point0, point1, RED)
image.draw_line(point0, point2, RED)
image.draw_line(point2, point1, RED)

#Letter H

point3 = Point(30, 10)
point4 = Point(30, 20)
point5 = Point(30, 15)
point6 = Point(40, 15)
point7 = Point(40, 10)
point8 = Point(40, 20)

image.draw_line(point3, point4, RED)
image.draw_line(point5, point6, RED)
image.draw_line(point7, point8, RED)


PPM.write(image)