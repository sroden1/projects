#! /usr/bin/python

from drawit import Image, PPM, Point, Color, RED, BLUE

image = Image()

SILVER = Color(192, 192, 192)
GOLD = Color (255, 215, 0)

L_center = Point(image.width/15, image.height/2)
L_radius = image.width / 42

Hilt0_center = Point(image.width/5, (image.height/2 + 100))
Hilt0_radius = image.width / 42

Hilt1_center = Point(image.width/5, (image.height/2 - 100))
Hilt1_radius = image.width / 42

blade_start = Point(L_center.x, L_center.y-(L_radius/2))
blade_end = Point(image.width/(1.8), image.height/2 + (L_radius/2))
blade_end1 = Point(image.width/(1.8), image.height/2 - (L_radius/2))

guard_start = Point(Hilt0_center.x - 10, Hilt0_center.y)
guard_end = Point(Hilt1_center.x +10, Hilt1_center.y)

blade_point = Point(image.width/1.05, image.height/2)

image.draw_rectangle(blade_start, blade_end, SILVER)
image.draw_circle(L_center, L_radius, GOLD)
image.draw_circle(Hilt0_center, Hilt0_radius, GOLD)
image.draw_circle(Hilt1_center, Hilt1_radius, GOLD)
image.draw_rectangle(guard_start, guard_end, GOLD)
image.draw_circle(Hilt1_center, Hilt1_radius/3, BLUE)
image.draw_circle(Hilt0_center, Hilt0_radius/3, BLUE)
image.draw_circle(L_center, L_radius/3, RED)
image.draw_line(blade_end, blade_point, SILVER)
image.draw_line(blade_end1, blade_point, SILVER)

for i in range(blade_end1.y, blade_end.y + 1):
	image.draw_line(Point(blade_end1.x, i), blade_point, SILVER)
	i += 1

PPM.write(image)
