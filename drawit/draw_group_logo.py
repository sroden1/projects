#! /usr/bin/python

from drawit import Image, PPM, Point, Color, WHITE, BLACK, RED, BLUE

image = Image()
radius = image.width / 8
center0 = Point(image.width/2, image.height - (image.height/4))
center1 = Point(image.width/2, image.height - (image.height/4) - radius - int(radius/(1.5)))
center2 = Point(image.width/2, image.height - (image.height/4) - radius - int(radius/(1.5)) - int(radius/(1.5))- int(radius/2))

image.draw_circle(center0, radius, WHITE)
image.draw_circle(center1, int(radius/(1.5)), WHITE)
image.draw_circle(center2, int(radius/2), WHITE)

point0 = Point(image.width/2 - int(radius/2), image.height - (image.height/4) - radius - int(radius/(1.5)) - int(radius/(1.5))- int(radius/2) - radius)
point1 = Point(image.width/2 + int(radius/2), image.height - (image.height/4) - radius - int(radius/(1.5)) - int(radius/(1.5))- int(radius/2) - int(radius/2))
image.draw_rectangle(point0, point1, RED)

point3 = Point(image.width/2 - int(radius/4), image.height - (image.height/4) - radius - int(radius/(1.5)) - int(radius/(1.5))- int(radius/2) - radius - int(radius/2))
point4 = Point(image.width/2 + int(radius/4), image.height - (image.height/4) - radius - int(radius/(1.5)) - int(radius/(1.5))- int(radius/2) - radius)
image.draw_rectangle(point3, point4, RED)

PPM.write(image)