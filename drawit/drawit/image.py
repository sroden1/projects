''' image.py: DrawIt Image '''

from color import Color
from point import Point

import math

class Image(object):
    DEFAULT_WIDTH  = 640
    DEFAULT_HEIGHT = 480

    def __init__(self, width=DEFAULT_WIDTH, height=DEFAULT_HEIGHT):
        ''' Initialize Image object's instance variables '''
        self.width = width
        self.height = height
        self.pixels = []

        for row in range(height):
            pixelrow = []
            for column in range(width):        
                pixelrow.append(Color()) 
            self.pixels.append(pixelrow)

    def __eq__(self, other):
        ''' Returns True if current instance and other object are equal '''
        if self.width == other.width and self.height == other.height and self.pixels == other.pixels:  #if doesn't work check here
            return True
        else:
            return False

    def __str__(self):
        ''' Returns string representing Image object '''
        return "Image(width={},height={})".format(self.width,self.height)

    def __getitem__(self, point):
        ''' Returns pixel specified by point

        If point is not with the bounds of the Image, then an IndexError is raised.
        '''
        if point.y in range(self.height) and point.x in range(self.width):
            return self.pixels[point.y][point.x]
        else:
            raise IndexError


    def __setitem__(self, point, color):
        ''' Sets pixel specified by point to given color

        If point is not with the bounds of the Image, then an IndexError is raised.
        '''
        if point.y in range(self.height) and point.x in range(self.width):
            self.pixels[point.y][point.x] = color
        else:
            raise IndexError

    def clear(self):
        ''' Clears Image by setting all pixels to default Color '''
        for y in range(self.height):
            for x in range(self.height):
                self.pixels[y][x] = Color()


    def draw_line(self, point0, point1, color):
        ''' Draw a line from point0 to point1 '''
        a = math.atan2(point1.y - point0.y, point1.x - point0.x)
        d = point0.distance_from(point1)
        if a >= math.pi: 
            a = a - math.pi

        place = 0
        while place <= d:
            x = int(point0.x + place*math.cos(a))
            y = int(point0.y + place*math.sin(a))
            self.pixels[y][x] = color    

            place += 1


    def draw_rectangle(self, point0, point1, color):
        ''' Draw a rectangle from point0 to point1 '''
        column_start = min(point0.x, point1.x)
        column_end = max(point0.x, point1.x)
        row_start = min(point0.y, point1.y)
        row_end = max(point0.y, point1.y)

        for row in range(row_start, row_end):
            for column in range(column_start, column_end):
                point = Point(column,row)
                self[point] = color


    def draw_circle(self, center, radius, color):
        ''' Draw a circle with center point and radius '''
        column_start = center.x - radius
        column_end = center.x + radius
        row_start = center.y - radius
        row_end = center.y + radius 

        for row in range(row_start, row_end + 1):
            for column in range(column_start, column_end + 1):
                point = Point(column,row)
                if point.distance_from(center) <= radius: 
                    self[point] = color

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
