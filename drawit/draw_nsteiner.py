#! /usr/bin/python

from drawit import Image, PPM, Point, Color, YELLOW, BLACK, RED

image = Image(300, 300)

point0 = Point(10, 10)
point1 = Point(10, 100)

image.draw_line(point0, point1, RED)


point2 = Point(10, 10)
point3 = Point(100, 100)

image.draw_line(point2, point3, RED)

point4 = Point(100, 100)
point5 = Point(100, 10)

image.draw_line(point4, point5, RED)

point6 = Point(150, 10)
point7 = Point(240, 10)

image.draw_line(point6, point7, RED)

point8 = Point(150, 10)
point9 = Point(150, 50)

image.draw_line(point8, point9, RED)

point10 = Point(150, 50)
point11 = Point(240, 50)

image.draw_line(point10, point11, RED)

point12 = Point(240, 50)
point13 = Point(240, 100)

image.draw_line(point12, point13, RED)

point14 = Point(150, 100)
point15 = Point(240, 100)

image.draw_line(point14, point15, RED)

PPM.write(image)