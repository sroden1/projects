/* Globals */

var PlaylistQueue   = [];	    /* List of songs	*/
var PlaylistIndex   = -1;	    /* Current song	*/
var CurrentNotebook = 'Search';	    /* Current notebook */

/* AJAX functions */

function requestJSON(url, callback) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
    	if (request.readyState == 4 && request.status == 200) {
    	    callback(JSON.parse(request.responseText));
	}
    }

    request.open('GET', url, true);
    request.send();
}

/* Display functions */

function displayTab() {
    /* TODO: Remove the active class from each notebook tab */
    var url = document.getElementsByClassName("nav-tabs")[0].getElementsByTagName("li");
    for (var i = 0; i < url.length; i++) {
        url[i].classList.remove("active");
    }


    /* TODO: Add active class to current object */
    this.classList.add("active");

    /* TODO: Check current object's id and call the appropriate display function */
    if (this.id === "search") {
        displaySearch();
    }
    else if (this.id === "artists") {
        displayArtist();
    }
    else if (this.id === "albums") {
        displayAlbum();
    }
    else if (this.id === "tracks") {
        displayTrack();
    }
    else if (this.id === "playlist") {
        displayPlaylist();
    }
}

    /* TODO: Request JSON from URL and then call appropriate render function */
function displayResults(url) {
    requestJSON(url, function(data) {
        if (data['render'] == 'gallery') {
            renderGallery(data['results'], data['prefix']);
        } else if (data['render'] == 'album') {
            renderAlbum(data['results']);
        } else if (data['render'] == 'track') {
            renderTrack(data['results']);
        }
    });
}


function displaySearch(query) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = 'Search';

    var element    = document.getElementById('notebook-body-nav');
    var renderHTML = '<form class="navbar-form text-center">';

    renderHTML += '<div class="form-group">';
    renderHTML += '<input type="text" placeholder="What to search for" name="query" id="query" onkeyup="updateSearchResults()" class="form-control">';
    renderHTML += '<select id="table" name="table" class="form-control" onchange="updateSearchResults()">';
    renderHTML += '<option value="Artists">Artists</option>';
    renderHTML += '<option value="Albums">Albums</option>';
    renderHTML += '<option value="Tracks">Tracks</option>';
    renderHTML += '</select>';
    renderHTML += '</div></form>';

    element.innerHTML = renderHTML;
    return updateSearchResults();

    
}

function displayArtist(artist) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = 'Artist';
    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */


    /* TODO: If artist is undefined, then set to empty string */
    if (artist===undefined) {
        artist='';
    }

    /* TODO: Call displayResults for appropriate /artist/ URL */
    displayResults('/artist/' + artist);
}

function displayAlbum(album) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = 'Album';
    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */

    /* TODO: If album is undefined, then set to empty string */
    if (album===undefined) {
        album='';
    }

    /* TODO: Call displayResults for appropriate /album/ URL */
    displayResults('/album/' + album);
}

function displayTrack(track) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = 'Track';

    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */

    /* TODO: If album is undefined, then set to empty string */
    if (track===undefined) {
        track='';
    }

    /* TODO: Call displayResults for appropriate /album/ URL */
    displayResults('/track/' + track);
}

function displayPlaylist() {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = 'Playlist';

    var element = document.getElementById("notebook-body-contents");

    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */
    renderHTML = '<table id="playlist" class="table table-striped table-bordered text-center">';
    renderHTML += '<thead><th>#</th><th>Album Cover</th><th>Artist</th><th>Album</th><th>Track Name</th></thead>';
   
     var table = document.getElementById("playlist");

     for (var i = 0; i < PlaylistQueue.length; i++) {
        var item = PlaylistQueue[i]
        renderHTML += '<tr>';
        renderHTML += '<td>' + (i+1) +'</td> <td>' + '<img width="50" src="' + item['albumImage'] + '">'  + "</td> <td>" + item['artistName'] + "</td> <td>" + item['albumName'] + "</td> <td>" + item['trackName'] + '</td>';
        renderHTML += '</tr>';
        }

     renderHTML += '</table>';


     element.innerHTML = renderHTML;
}
/* Render functions */

function renderGallery(data, prefix) {

    var element    = document.getElementById('notebook-body-contents');
    var renderHTML = '';

    renderHTML += '<div class="row">';

    for (var i=0; i < data.length; i++) {
        var item = data[i];
        var id = item[0];
        var name = item[1];
        var image = item[2];
        if (i % 4 == 0) {
            renderHTML += '</div>';
            renderHTML += '<div class="row">';
        }
        renderHTML += '<div class="col-xs-3">';
        renderHTML += '<a href="#" onclick="display' + prefix + '(' + id + ')' + '" class="thumbnail text-center">';
        renderHTML += '<img src=' + image + ' height="200" width="200">';
        renderHTML += '<div class="caption"><h4>' + name + '</h4></div>';
        renderHTML += '</a>';
        renderHTML += '</div>';
    }
    renderHTML += '</div>';

    element.innerHTML = renderHTML;
}

function renderAlbum(data) {
    /* TODO: Set notebook-body-contents to table of tracks
     *
     *	- Each track should have an icon anchor for adding the song to playlist
     *	or for playing the song.
     *
     */
  var element    = document.getElementById('notebook-body-contents');
  var renderHTML = '';

  renderHTML += '<table class="table table-striped">';
  renderHTML += '<thead>';
  renderHTML += '<th>Number</th>';
  renderHTML += '<th>Name</th>';
  renderHTML += '<th>Actions</th>';
  renderHTML += '</thead>';
  renderHTML += '<tbody>';
  for (var i=0; i < data.length; i++) {
    var item = data[i];
    var TrackID = item[0];
    var Number = item[1];
    var Name = item[2];
    renderHTML += '<tr>';
    renderHTML += '<td>' + Number + '</td>';
    renderHTML += '<td><a href="#" onclick="displayTrack('+ TrackID + ')">' + Name + '</a></td>';
    renderHTML += '<td>';
    renderHTML += '<a href="#" onclick="addSong(' + TrackID + ')"><i class="fa fa-plus"></i></a> ';
    renderHTML += '<a href="#" onclick="playSong(' + TrackID + ')"><i class="fa fa-play"></i></a>';
    renderHTML += '</td>';
    renderHTML += '</tr>';
    }
  renderHTML += '</tbody>';
  renderHTML += '</table>';

element.innerHTML = renderHTML;

}

function renderTrack(data) {
  var element    = document.getElementById('notebook-body-contents');
  var renderHTML = '';

    var TrackID = data[0];
    var ArtistID = data[1];
    var ArtistName = data[2];
    var AlbumID = data[3];
    var AlbumName = data[4];
    var TrackNumber = data[5];
    var TrackName = data[6];
    var AlbumImage = data[7];
    renderHTML += '<div class="row">';
    renderHTML += '<div class="col-sm-3">';
    renderHTML += '<a href="#" onclick="playSong(' + TrackID + ')" class="thumbnail text-center"><img src=" ' + AlbumImage + ' "></a>';
    renderHTML += '</div>';
    renderHTML += '<div class="col-sm-6">';
    renderHTML += '<dl>';
    renderHTML += '<dt>Track ID</dt>';
    renderHTML += '<dd>'+ TrackID + '</dd>';
    renderHTML += '<dt>Artist</dt>';
    renderHTML += '<dd><a href="#" onclick="displayArtist(' + ArtistID + ')">'+ ArtistName +'</a></dd>';
    renderHTML += '<dt>Album</dt>';
    renderHTML += '<dd><a href="#" onclick="displayAlbum(' + AlbumID + ')">' + AlbumName + '</a></dd>';
    renderHTML += '<dt>Track Number</dt>';
    renderHTML += '<dd> '+ TrackNumber + ' </dd>';
    renderHTML += '<dt>Track Name</dt>';
    renderHTML += '<dd>' + TrackName + '</dd>';
    renderHTML += '</dl>';
    renderHTML += '</div>';
    renderHTML += '</div>';

  element.innerHTML = renderHTML;
}

/* Update functions */

function updateSearchResults() {
    /* TODO: Construct search URL from query and table values and call
     * displayResults
     */

     var query = document.getElementById('query').value;
     var table = document.getElementById('table').value;
     var url = "/search?query=" + query + "&table=" + table

     displayResults(url);
}

function updateSongInformation() {
    
    var element = PlaylistQueue[PlaylistIndex];

    var albumImage = document.getElementById("albumImage");
    var player = document.getElementById("player");
    var trackName = document.getElementById("trackName");
    var albumName = document.getElementById("albumName");
    var artistName = document.getElementById("artistName");

    trackName.innerHTML = element.trackName;
    albumName.innerHTML = element.albumName;
    artistName.innerHTML = element.artistName;

    albumImage.src = element.albumImage;
    player.src = element.songURL;

    if (CurrentNotebook == "Playlist") {
      displayPlaylist();
     }
}

/* Audio controls */

function addSong(trackId, select) {
    var url = "/song/" + trackId;

    requestJSON(url, function(data) {
	var song = data['song'];
	PlaylistQueue.push(song);

	if (select !== undefined && select) {
	    selectSong(PlaylistIndex + 1);
	}
    });
}

function prevSong() {
    if (PlaylistIndex < 0 || PlaylistQueue.length == 0) {
    	return;
    }

    selectSong((PlaylistIndex + PlaylistQueue.length - 1) % PlaylistQueue.length);
}

function nextSong() {
    if (PlaylistIndex < 0 || PlaylistQueue.length == 0) {
    	return;
    }

    selectSong((PlaylistIndex + 1) % PlaylistQueue.length);
}

function selectSong(index) {
    PlaylistIndex = index;
    updateSongInformation();
    playSong();
}

function playSong(trackId) {
    var player     = document.getElementById('player');
    var playButton = document.getElementById('playButton');

    if (trackId !== undefined && trackId != '') {
    	return addSong(trackId, true);
    }

    if (PlaylistIndex < 0 && PlaylistQueue.length) {
    	return selectSong(0);
    }

    if (player.src === undefined || player.src == '') {
    	return;
    }

    if (player.ended || player.paused) {
    	playButton.innerHTML = '<i class="fa fa-pause"></i>';
	player.play();
    } else {
    	playButton.innerHTML = '<i class="fa fa-play"></i>';
	player.pause();
    }
}

function togglePlaySong() {
    playSong('');
}

function endSong() {
    document.getElementById('playButton').innerHTML = '<i class="fa fa-play"></i>';
    nextSong();
}

/* Registrations */
window.onload = function() {
    /* TODO: Register displayTab function for each tab's onclick callback */
    var url = document.getElementsByClassName("nav-tabs")[0].getElementsByTagName("li");
    for (var i = 0; i < url.length; i++) {
        url[i].onclick=displayTab;
    }

    document.getElementById('prevButton').onclick=prevSong;
    document.getElementById('playButton').onclick=togglePlaySong;
    document.getElementById('nextButton').onclick=nextSong;


    /* TODO: Register endSong function for player's onended callback */
    document.getElementById("player").onended=endSong;

    /* TODO: Display search results */
    displaySearch();
}

